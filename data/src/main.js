"use strict";

var bot = new (require("discord.js").Client)();
var last_good;

bot.on("ready", function (y) {
});

bot.on("message", function (m) {
	require("./patterns").forEach(function (pat) {
		var match = pat[0].call(m.content, pat[1]);
		if (match) {
			pat[2].call(m, m, match);
		}
	})
});

bot.on("messageDelete", function (m) {
	m.reply((m.editedAt || m.createdAt).toISOString() + "\n" + m.content);
});
bot.on("messageUpdate", function (m, n) {
	if (n.createdAt - m.createdAt > 300)
		bot.emit("messageDelete", m);
});

bot.on("raw", function (m) {
	switch (m.t) {
		case "MESSAGE_DELETE":
		case "MESSAGE_UPDATE":
			bot.channels.get(m.d.channel_id).send(m.t + " " + m.d.id);
			break;
	}
});

bot.login(require("fs").readFileSync("keys/bot",{encoding:"ascii"}).trim())
