var last_nonce = new Date()/1000|0;
var replyError = m => e => m.reply(e);

module.exports = [
	[x=>x, true, m => {
		var extra = { id: m.id };
		if (m.attachments.size > 0)
			extra.attachments = m.attachments.map(e => e.url).join(" ");
		console.log(JSON.stringify([
			m.createdAt,
			[m.channel.guild.id, m.channel.guild.name],
			[m.channel.id, m.channel.name],
			[m.author.id, m.author.username, m.author.discriminator],
			m.content,
			extra
		]));
	}],

	["".startsWith, "untrusted comment: signature from eval secret key\n", m => {
		var spawn = require("child_process").spawn;
		var signify = spawn("signify", ["-Veqx-", "-m-", "-pkeys/pub"])
		signify.stdin.write(m.content + "\n");
		signify.stdin.end();

		var buf, addbuf = d => buf = buf == null ? d : Buffer.concat([buf,d]);
		signify.stdout.on("data", addbuf);
		signify.stderr.on("data", addbuf);
		signify.on("close", s => {
			var nonce = parseInt(buf);
			if (s > 0)
				;
			else if (!(nonce > last_nonce)) /* not <= if NaN */
				buf = "もう手遅れ" + last_nonce;
			else try {
				last_nonce = nonce;
				buf = eval(""+buf);
			}
			catch (e) {
				buf = e;
			}
			m.reply(""+buf);
		});
	}],

	["".match, /^通常攻撃\s*([^]+)/, (m, match) => {
		m.channel.setTopic(match[1]).then(ch => {
			m.reply("二回攻撃「" + match[1] + "」");
		}, replyError(m));
	}],
];
