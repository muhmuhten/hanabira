#!/usr/bin/env node

var PER_FETCH = 100;
var bot = new (require("discord.js").Client)();
var spawn = require("child_process").spawn;
var last_nonce = new Date()/1000|0

bot.on('ready', function (y) {
	console.log("// ex: se ft=javascript :");
	var awaited = 0;
	this.channels.forEach(function (e,k,m) {
		if (!e.fetchMessages)
			return;
		awaited++;
		e.fetchMessages({limit: PER_FETCH}).then(function (g) {
			console.log(g);
			g.forEach(function (y) {
				y.delete().then(console.log);
			});
		});
	});
	console.log("a thing");
});


bot.login(require("fs").readFileSync("keys/bot",{encoding:"ascii"}).trim())
