#!/usr/bin/env node

var bot = new (require("discord.js").Client)();
var spawn = require("child_process").spawn;

var nonce = new Date()/1000|0;

var args = process.argv.slice(2);
if (args.length <= 0)
	args = ['((q,p)=>((c,v,u)=>((s=>{c[v]=u;try{q(p)[0]||it;u="ok"}catch(e){c[v]=s;u=e}})(c[v]),u))(q.cache,q.resolve(p)))(require,"./patterns")'];

bot.on("ready", function (y) {
	var awaiting = 0;
	args.forEach(function (s) {
		awaiting++;
		var signify = spawn("signify", ["-Sex-", "-m-", "-skeys/sec"])
		signify.stdin.write(nonce++ + ";" + s + "\n");
		signify.stdin.end();

		var buf;
		function addbuf(d) {
			buf = buf == null ? d : Buffer.concat([buf,d]);
		}
		signify.stdout.on("data", addbuf);
		signify.stderr.on("data", addbuf);

		signify.on("close", function (s) {
			process.stdout.write(buf);
			bot.channels.get("331933267445940226").send(""+buf).then(function () {;
				if (--awaiting <= 0)
					process.exit(0);
			});
		});
	});
});

bot.login(require("fs").readFileSync("keys/bot",{encoding:"ascii"}).trim());
